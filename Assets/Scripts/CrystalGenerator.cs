﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalGenerator {

	Mesh GenerateCrystalFromCrossSection(CrystalCrossSection crystalDesign) {
		Mesh mesh = new Mesh();
		
		//Vertices
		List<Vector3> verts = new List<Vector3>();

		{
			//Outer Verts
			foreach(var vert in crystalDesign.vertices) {
				verts.Add((Vector3)vert + Vector3.up / 2f);
				verts.Add((Vector3)vert + Vector3.down / 2f);
			}

		}		


		//Triangles


		//UVs

		return mesh;
	}
}

public class CrystalCrossSection {
	public Vector2[] vertices;
	public CrystalType type;
}