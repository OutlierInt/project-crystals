﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour {

	public float speed;
	public LayerMask layerMask;
	public float lifeTime;
	public bool drawDebugPathLines;

	public int maxBounces = 3;
	private int maxBouncesInit;
	public Color initColor, weakColor;


	private void Awake() {
		Destroy(gameObject, lifeTime);
		maxBouncesInit = maxBounces;

		var trail = GetComponent<TrailRenderer>();
		if (trail != null)
			trail.material.SetColor("_TintColor", initColor);
	}

	void Update() {
		if(Input.GetKeyDown(KeyCode.R)) {
			Destroy(gameObject);
		}


		float travelDistance = speed * Time.deltaTime;
		int safetyIterations = 10;

		Physics.queriesHitBackfaces = true;

		while(travelDistance > 0 || safetyIterations-- <= 0) {
			RaycastHit hit;
			//Hit something while moving...
			if (Physics.Raycast( transform.position, transform.forward, out hit, travelDistance, layerMask )) {
				if(drawDebugPathLines)
					Debug.DrawLine(transform.position, hit.point, Color.cyan, 3f);

				transform.position = hit.point;
				travelDistance -= hit.distance;

				//Hit Crystal
				var crystal = hit.transform.GetComponent<Crystal>();
				if(crystal != null) {
					crystal.Mark();
					Vector3 normal = hit.normal * (Vector3.Dot(transform.forward, hit.normal) > 0 ? 1 : -1);
					transform.rotation = Quaternion.LookRotation(normal);
					transform.position += transform.forward * 0.001f;
				}
				//Reflect
				else {
					if(--maxBounces < 0) {
						Destroy(gameObject);
					}
					else {
						Vector3 newDirection = Vector3.Reflect(transform.forward, hit.normal);
						transform.rotation = Quaternion.LookRotation(newDirection);
						transform.position += transform.forward * 0.001f;

						var trail = GetComponent<TrailRenderer>();
						if(trail != null) {
							trail.material.SetColor("_TintColor", Color.Lerp(weakColor, initColor, (float)maxBounces/maxBouncesInit));
						}
					}
				}
			}
			//Hit nothing. Continue.
			else {
				var newPosition = transform.position + transform.forward * travelDistance;
				if(drawDebugPathLines)
					Debug.DrawLine(transform.position, newPosition, Color.cyan, 3f);
				transform.position = newPosition;
				travelDistance = 0;
			}
		}

		Physics.queriesHitBackfaces = false;
	}



}
