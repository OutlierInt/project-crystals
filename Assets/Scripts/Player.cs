﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour {

	private Rigidbody rb;
	private NavMeshAgent navAgent;

	public float speed;
	public Laser laserObj;
	public bool drawDebugLines;
	public Material lineMaterial;

	[Header("Spreadshot")]
	public int spreadShots = 1;
	public float spreadAngle = 15f;

	void Awake() {
		rb = GetComponent<Rigidbody>();
		navAgent = GetComponent<NavMeshAgent>();
	}

	void Update() {

		//Movement
		Vector3 movement = new Vector3();
		if(Input.GetKey(KeyCode.W)) movement += Vector3.forward;
		if(Input.GetKey(KeyCode.S)) movement += Vector3.back;
		if(Input.GetKey(KeyCode.A)) movement += Vector3.left;
		if(Input.GetKey(KeyCode.D)) movement += Vector3.right;
		movement = Vector3.ClampMagnitude(movement, 1f);
		rb.velocity = movement * speed;

		//Shoot
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Plane playerPlane = new Plane(Vector3.up, transform.position);
		float enter;

		if (playerPlane.Raycast(ray, out enter)) {
			Vector3 hitPoint = ray.GetPoint(enter);

			if(drawDebugLines)
				Debug.DrawLine(transform.position, hitPoint, Color.white);

			if(Input.GetMouseButtonDown(0)) {

				Vector3 mouseAimDirection = (hitPoint - transform.position).normalized;
				for(int i = 0; i < spreadShots; i++) {
					var adjustAimDirection = Quaternion.AngleAxis( Mathf.Lerp(-spreadAngle/2f, spreadAngle/2f, ((float)i)/(Mathf.Max(spreadShots - 1, 1))), Vector3.up ) * mouseAimDirection;
					FireLaser( adjustAimDirection, transform.position );
				}
				//FireLaser( mouseAimDirection, transform.position );

			}
		}

		//Pathfinding
		if(navAgent != null) {
			if(Input.GetMouseButtonDown(1)) {
				if (playerPlane.Raycast(ray, out enter)) {
					Vector3 hitPoint = ray.GetPoint(enter);
					NavMeshPath path = new NavMeshPath();
					navAgent.CalculatePath(hitPoint, path);
					navAgent.SetPath(path);
				}
			}
			if(navAgent.remainingDistance < 0.001f) {
				navAgent.ResetPath();
			}
		}

	}

	public void FireLaser(Vector3 aimDirection, Vector3 position) {
		Quaternion aimRotation = Quaternion.LookRotation(aimDirection);
		Instantiate(laserObj, position /*+ aimDirection * 1.5f*/, aimRotation);
	}
}