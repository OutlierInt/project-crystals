﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CrystalType { Refract, Reflect }

[RequireComponent(typeof(MeshRenderer))]
public class Crystal : MonoBehaviour {

	public bool marked = false;
	private new Renderer renderer;
	private Material initMaterial;

	private void Awake() {
		renderer = GetComponent<Renderer>();
		initMaterial = new Material(renderer.material);
	}

	private void Update() {
		if(Input.GetKeyDown(KeyCode.R)) {
			Unmark();
		}
	}

	public void Mark() {
		marked = true;
		renderer.material.color = new Color(1,0,1, renderer.material.color.a);
	}

	public void Unmark() {
		marked = false;
		renderer.material = initMaterial;
	}

}
