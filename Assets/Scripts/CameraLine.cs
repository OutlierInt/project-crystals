﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CameraLine : MonoBehaviour {

	public Player player;
	public Material lineMaterial;
	private NavMeshAgent navAgent;

	void Awake() {
		navAgent = player.GetComponent<NavMeshAgent>();
	}

	void OnPostRender() {
		//Draw Aim Line
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		Plane playerPlane = new Plane(Vector3.up, player.transform.position);
		float enter;
		if (playerPlane.Raycast(ray, out enter)) {
			Vector3 hitPoint = ray.GetPoint(enter);
			DrawLine(player.transform.position, hitPoint, lineMaterial, Color.white);
		}

		//Player Pathfinding
		if(navAgent != null) {
			if(navAgent.path != null) {
				var corners = navAgent.path.corners;
				for(int i = 0; i < corners.Length-1; i++){
					DrawLine(corners[i], corners[i+1], lineMaterial, Color.green);
				}
			}
		}
	}

	public void DrawLine(Vector3 startVertex, Vector3 endVertex, Material mat, Color col) {
		//GL.PushMatrix();
		//GL.LoadOrtho();
		GL.Begin(GL.LINES);
		mat.SetPass(0);
		GL.Color(col);
		GL.Vertex(startVertex);
		GL.Vertex(endVertex);
		GL.End();
		//GL.PopMatrix();
	}
}
